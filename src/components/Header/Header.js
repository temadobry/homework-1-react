import React, {Component} from 'react';
import styles from "./Header.module.scss"
import CartCVG from "../CartSVG/CartCVG";
import FavoriteCVG from "../FavoriteSVG/FavoriteCVG";
import {PropTypes} from "prop-types"

class Header extends Component {
    render() {
        const {counterItems} =this.props
        return (
            <div className={styles.top}>
                <h2>    <CartCVG/>   {counterItems}</h2>
                <h2>    <FavoriteCVG/>   {counterItems}</h2>
            </div>
        );
    }
}


Header.propTypes ={
    counterItems:PropTypes.number

}

Header.defaultProps = {
    counterItems: 0,
}


export default Header;