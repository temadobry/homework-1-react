import React from 'react';
import styles from'./App.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import Card from "./components/Card/Card";
import CardList from "./components/CardList/CardList";
import Header from "./components/Header/Header";


class App extends React.Component {
  state = {
      firstModalStatus : false,
      secondModalStatus: false,
      card : [],
      counterItems : 0
}

async componentDidMount() {
      const {data} = await fetch("shop.json").then(res=>res.json())
    this.setState({card:data})
    const counterItems = localStorage.getItem("counterItems")
    this.setState({counterItems: JSON.parse(counterItems)})

}
    toggleModal = (value) => {
        this.setState({ firstModalStatus: value })

    }

    increaseCounterItem = () => {
      this.setState({counterItems: this.state.counterItems + 1})
        localStorage.setItem("counterItems", JSON.stringify(this.state.counterItems + 1))
        console.log(this.state.counterItems)

    }




  render() {



      const{ firstModalStatus,secondModalStatus,card, counterItems} = this.state


    return (

        <div className={styles.App}>

            <Header counterItems ={counterItems}></Header>


            <main>

            <CardList card={card} toggleModal={this.toggleModal} ><Card/></CardList>

            </main>
            {
                firstModalStatus &&
                <Modal
                    incrementCartItem ={this.incrementCartItem}

                    toggleModal={this.toggleModal}
                    header={"Do you wanna add this item?             "}
                    closeButton={true}
                    text={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, veniam.'}
                    action={
                        <div>
                            <Button onClick={()=>{this.increaseCounterItem()}} text="YES" backgroundColor="green"></Button>
                            <Button onClick={()=>{this.toggleModal(false)}} text="No" backgroundColor="red"></Button>
                        </div>
                    }
                >
                </Modal>


            }




        </div>
    );

  }

    }
    // handleClickSecond = () =>{
    //     this.setState({
    //         secondModalStatus: !this.state.secondModalStatus,
    //
    //     }
    //
    //     )
    // }

export default App;
